{{/*
Expand the name of the chart.
*/}}
{{- define "openldap.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a fully qualified app name for slapd
*/}}
{{- define "openldap.slapd.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-slapd" $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create a fully qualified app name for ldapadmin
*/}}
{{- define "openldap.ldapadmin.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-ldapadmin" $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "openldap.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "openldap.labels" -}}
helm.sh/chart: {{ include "openldap.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
slapd common labels
*/}}
{{- define "openldap.slapd.labels" -}}
{{- include "openldap.slapd.selectorLabels" . }}
{{ include "openldap.labels" . }}
{{- end }}

{{/*
ldapadmin common labels
*/}}
{{- define "openldap.ldapadmin.labels" -}}
{{- include "openldap.ldapadmin.selectorLabels" . }}
{{ include "openldap.labels" . }}
{{- end }}

{{/*
Common selector labels
*/}}
{{- define "openldap.selectorLabels" -}}
app.kubernetes.io/instance: {{ include "openldap.name" . }}
{{- end }}

{{/*
slapd selector labels
*/}}
{{- define "openldap.slapd.selectorLabels" -}}
app.kubernetes.io/name: {{ include "openldap.slapd.fullname" . }}
app.kubernetes.io/component: "ldapdb"
app.kubernetes.io/part-of: {{ include "openldap.name" . }}
{{- end }}

{{/*
ldapadmin selector labels
*/}}
{{- define "openldap.ldapadmin.selectorLabels" -}}
app.kubernetes.io/name: {{ include "openldap.ldapadmin.fullname" . }}
app.kubernetes.io/component: "adminportal"
app.kubernetes.io/part-of: {{ include "openldap.name" . }}
{{- end }}

{{/*
generate list of slapd multi-master hosts
*/}}
{{- define "openldap.slapd.hosts" -}}
{{- $hosts := "" -}}
{{- $hostCount := .Values.slapd.replicas | int -}}
{{- range until $hostCount -}}
{{- $hostName := printf "%s-slapd-%d.%s-slapd.%s.svc.cluster.local" $.Chart.Name . $.Chart.Name $.Values.cluster.namespace -}}
{{- $hosts = printf "%s %s" $hostName $hosts -}}
{{- end -}}
{{- printf "\"%s\"" $hosts -}}
{{- end }}
