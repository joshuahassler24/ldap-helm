apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "openldap.slapd.fullname" . }}
  labels:
    {{- include "openldap.slapd.labels" . | nindent 4 }}
spec:
  {{- if .Values.slapd.storage.enabled }}
  volumeClaimTemplates:
  - metadata:
      name: data
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: {{ .Values.slapd.storage.size }}
      storageClassName: {{ .Values.slapd.storage.storageClass }}
  {{- end }}
  replicas: {{ .Values.slapd.replicas }}
  selector:
    matchLabels:
      {{- include "openldap.slapd.selectorLabels" . | nindent 6 }}
  serviceName: {{ .Chart.Name }}-slapd
  {{- if gt $.Values.slapd.replicas 1.0 }}
  updateStrategy:
    type: RollingUpdate
  {{- end }}
  template:
    metadata:
      {{- with .Values.slapd.Annotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "openldap.slapd.selectorLabels" . | nindent 8 }}
    spec:
      {{ if .Values.slapd.tls.secret }}
      initContainers:
        - name: import-tls-secret
          image: alpine:3.13
          command: ["sh", "-c", "cp /secret/* /tls"]
          volumeMounts:
          - mountPath: /secret
            name: tls-secret
            readOnly: true
          - mountPath: /tls
            name: tls
      {{- end }}
      setHostnameAsFQDN: true
      containers:
        - name: {{ .Chart.Name }}-slapd
          image: "{{ .Values.slapd.image.repository }}:{{ .Values.slapd.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.slapd.image.pullPolicy }}
          ports:
            - name: ldap
              containerPort: 389
              protocol: TCP
            {{- if .Values.slapd.env.LDAP_TLS }}
            - name: ldaps
              containerPort: 636
              protocol: TCP
            {{- end }}
          {{- with .Values.slapd.env }}
          env:
          {{- range $key, $value := . }}
          - name: {{ $key }}
            value: "{{ $value }}"
          {{- end }}
          {{- end }}
          {{- if gt $.Values.slapd.replicas 1.0 }}
          - name: LDAP_REPLICATION
            value: "true"
          - name: LDAP_REPLICATION_CONFIG_SYNCPROV
            value: "binddn=\"cn=admin,cn=config\" bindmethod=simple credentials=\"$LDAP_CONFIG_PASSWORD\" searchbase=\"cn=config\" type=refreshAndPersist retry=\"60 +\" timeout=1 starttls=critical"
          - name: LDAP_REPLICATION_DB_SYNCPROV
            value: "binddn=\"cn=admin,$LDAP_BASE_DN\" bindmethod=simple credentials=\"$LDAP_ADMIN_PASSWORD\" searchbase=\"$LDAP_BASE_DN\" type=refreshAndPersist interval=00:00:00:10 retry=\"60 +\" timeout=1 starttls=critical"
          - name: LDAP_REPLICATION_HOSTS
            value: {{ include "openldap.slapd.hosts" . }}
          {{- end }}
          volumeMounts:
          - name: data
            mountPath: "/etc/ldap/slapd.d"
            subPath: "slapd.d"
          - name: data
            mountPath: "/var/lib/ldap"
            subPath: "ldap"
          {{- if .Values.slapd.tls.secret }}
          - name: tls
            mountPath: "/container/service/slapd/assets/certs"
          {{- end }}
          livenessProbe:
              tcpSocket:
                port: 389
              initialDelaySeconds: 400
              periodSeconds: 10
              failureThreshold: 10
          readinessProbe:
              tcpSocket:
                port: 389
              initialDelaySeconds: 400
              periodSeconds: 10
              failureThreshold: 10
          {{- if .Values.slapd.resources }}
          resources:
            {{- toYaml .Values.slapd.resources | nindent 12 }}
          {{- end }}
          args:
            - "-l"
            - "{{ .Values.slapd.loglevel }}"
      {{ if .Values.slapd.tls.secret }}
      volumes:
      - name: tls-secret
        secret:
          secretName: {{ .Values.slapd.tls.secret }}
      - name: tls
        emptyDir:
          sizeLimit: "100M"
      {{- end }}
      {{- with .Values.slapd.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.slapd.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
